import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { Route, BrowserRouter, Redirect} from "react-router-dom";
import StartScreen from './Components/startScreen/Startscreen';
import Dashboard from './Components/mainApp/Dashboard';
import { getCurrentDayName } from './Main';
import GenerateSchedule from './Components/mainApp/GenerateSchedule';
import FullRecipe from './Components/mainApp/FullRecipe';

let routing = "";

if(window.localStorage.getItem("User") === null) {
    routing = (
        <BrowserRouter>
            <Route path="/" exact component={StartScreen}/>
            <Route path="/:action" exact component={StartScreen} />
            <Route path="/activate/:token" exact component={StartScreen} />
            <Route path="/password/reset/:resettoken" exact component={StartScreen} />
        </BrowserRouter>
    )
}

if(window.localStorage.getItem("User") !== null) {
    routing = (
        <BrowserRouter>
                <Redirect from="/" exact to={"/recipes/filter="+getCurrentDayName()} />
                <Route path="/:component" exact component={Dashboard}/>
                <Route path="/:component/filter=:filter" exact component={Dashboard} />
                <Route path="/schedule/new" exact component={GenerateSchedule} />
                <Route path="/recipe/:id" exact component={FullRecipe} />
        </BrowserRouter>
    )
}

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
