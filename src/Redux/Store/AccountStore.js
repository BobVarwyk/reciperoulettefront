import { createStore } from 'redux';
import accountReducer from '../Reducers/AccountCredentialsReducer';

export default createStore(accountReducer);