import React from 'react';

function OutputResponseMessage(props) {
    return (
        // Check if the props children is filled, if not.. don't show the message
        props.children !== "" ? (
            <div className={ props.succes ? "succesful" : "failed"} id="response-msg">
                    <span> {props.children} </span>
                    <div className="close-msg"> <button value="" name="errorMessage" onClick={props.handler}> <i className="fal fa-times"></i> </button> </div>
            </div>
        ) : (
            null
        )
    );
}

export default OutputResponseMessage;