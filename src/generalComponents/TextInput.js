import React from 'react';

const TextInput = (props) => {
    return (
        <div className="outer-container">
            <input autoComplete="off" value={props.value} name={props.name} onChange={props.handlechange} type={props.type} />
            <span className="error-dsp" id={props.name+"-error-dsp"}> {props.error} </span>
        </div>
    );
}

export default TextInput;