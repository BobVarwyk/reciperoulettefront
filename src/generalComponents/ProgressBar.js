import React from 'react';

class ProgressBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentStep: this.props.currentStep
        }
    }

    componentWillReceiveProps(props) {
        this.setState({
            currentStep: props.currentStep
        }, () => {
            if(this.props.currentStep < 4) {
                const UL = document.getElementById("steps");
                const tags = UL.childNodes;
                for(var i = 0; i < this.state.currentStep; i++) {
                    tags[i].classList.add("active");
                }
            }
        })
    }

    componentDidMount() {
        if(this.props.currentStep < 4) {
            const UL = document.getElementById("steps");
            const tags = UL.childNodes;
            for(var i = 0; i < this.state.currentStep; i++) {
                tags[i].classList.add("active");
            }
        }
    }


    render() {

        if(this.state.currentStep === 4) {
            return null;
        }

        return (
            <div className="progress-bar">
                <ul className="steps" id="steps">
                    <li> <i className="fas fa-user"> </i> </li>
                    <li> <i className="fal fa-wheat"></i> </li>
                    <li> <i className="fal fa-home-lg-alt"></i> </li>
                </ul>
            </div>
        );
    }
  }

  export default ProgressBar;