// FRONT-END check on input fields
export function validateFormFields({firstName, lastName, email, password, passwordConfirm}) {
    // Email regex for valid email
    var validEmailFormat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    // Password regex for one uppercase, one lowercase and one number and between 8 and 40 characters
    var validPasswordFormat = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,40}$/;
    var errors = {
        firstName:
            !firstName || firstName.trim().length === 0
            ? "Your first name is mandatory"
            : null,
        lastName:
            !lastName || lastName.trim().length === 0
            ? "Your last name is mandatory"
            : null,
        email:
            !email || email.trim().length === 0 || !validEmailFormat.test(email)
            ? "Your email isn't correct."
            : null,
        password:
            !password || password.trim().length === 0 || !validPasswordFormat.test(password)
            ? "Passwords needs to contain one uppercase, one digit and one special character"
            : null,  
        passwordConfirm:
            !passwordConfirm || passwordConfirm !== password
            ? "Passwords don't match"
            : null
        }
        return errors;
}


// Function to return current day name for filtermenu component
export function getCurrentDayName() {
    const days = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"]
    let day = new Date();
    return days[day.getDay()];
}