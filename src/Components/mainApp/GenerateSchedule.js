import React from 'react';
import { NavLink } from "react-router-dom";
import { getCurrentDayName } from '../../Main';
import Axios from 'axios';
import {ReactComponent as RouletteWheel} from '../../images/roulette.svg';
import OutputResponseMessage from '../../generalComponents/OutputResponseMessage';
import { AnimateOnChange } from 'react-animation';

class GenerateSchedule extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: JSON.parse(window.localStorage.getItem("User")),
            generationSuccesful: false,
            error: false,
            errorMessage: ""
        }
        this.handleGenerateCall = this.handleGenerateCall.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleGenerateCall() {
        // Set the animation class on the roulette wheel
        document.getElementsByClassName("roulette-svg")[0].classList.add("spin-animate");

        // Send the request to the API which returns a response (true or false)
        Axios.get(`${process.env.REACT_APP_BACKEND_API}/recipes/generate/${this.state.user.id}`)
        .then(response => {
            if(response.data.succes) {
                setTimeout(() => {
                    this.handleGenerationState(true)
                }, 1500);
            } else {
                this.handleErrorState(response.data.succes, response.data.message)
            }
        });
    }

    handleGenerationState(status) {
        this.setState({
            generationSuccesful: status
        })
    }

    handleChange(event) {
        const {name, value} = event.target;
        // Set the state with the values in input boxes
        this.setState({
            [name]: value
        })
    }

    handleErrorState(status, message) {
        this.setState({
            error: status,
            errorMessage: message
        })
    }

    render() {
        return (
            <div className="generation-screen">
                <NavLink className="to-home" to={"/recipes/filter="+getCurrentDayName()}> <i className="far fa-times"></i> </NavLink>
                <div className="centered">
                    {!this.state.generationSuccesful ? (
                        <AnimateOnChange>
                            <div>
                                <h2> {this.state.user.firstName}, <br /> Spin the roulette wheel to pick seven new recipes! </h2>
                                <div className="roulette-wheel">
                                    <RouletteWheel />
                                    <div onClick={this.handleGenerateCall} className="roulette-trigger">
                                        <h2> Spin me! </h2>
                                    </div>
                                </div>
                            </div>
                        </AnimateOnChange>
                    ) : (
                        <AnimateOnChange>
                            <div>
                                <h2> {this.state.user.firstName}, <br /> we have selected seven new recipes for you! </h2>
                                <p> Take a look at them on the recipe page! </p>
                                <NavLink to={"/recipes/filter="+getCurrentDayName()} className="btn"> See what's in store for you! </NavLink>
                            </div>
                        </AnimateOnChange>
                    )}
                </div>
                <OutputResponseMessage succes={this.state.error} handler={this.handleChange}>
                    {this.state.errorMessage}
                </OutputResponseMessage>
            </div>
        );
    }
}

export default GenerateSchedule;