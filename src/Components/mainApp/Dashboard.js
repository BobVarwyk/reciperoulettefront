import React from 'react';
import './App.css';
import MenuBar from './compositionComponents/MenuBar';
import HeaderBox from './compositionComponents/HeaderBox';
import Menu from './compositionComponents/Menu';
import Recipes from './pageComponents/Recipes';
import Allergies from './pageComponents/Allergies';
import Diets from './pageComponents/Diets';
import Groceries from './pageComponents/Groceries';
import Settings from './pageComponents/Settings';



class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: JSON.parse(window.localStorage.getItem("User")).id,
            user: JSON.parse(window.localStorage.getItem("User")),
            showMenu: false,
            activeComponent: "",
            pageTitle: ""
        }
        this.handleMenu = this.handleMenu.bind(this);
    }

    componentDidMount() {
        this.assignComponent(this.props.match.params.component)
    }

    componentWillReceiveProps(props) {
        this.assignComponent(props.match.params.component)
    }

    handleMenu() {
        if(this.state.showMenu) {
            document.getElementById("main-menu").classList.add("fade-out");
            setTimeout(() => {
                this.setState({
                    showMenu: false
                }) 
            }, 175);
        } else {
            this.setState({
                showMenu: true
            })
        }
    }

    assignComponent(URL) {
        switch(URL) {
            case "recipes":
                this.handleComponentSwitch("recipes", "Your recipes for this week")
            break;
            case "allergies":
                this.handleComponentSwitch("allergies", "Your allergies")
            break;
            case "diets":
                this.handleComponentSwitch("diets", "Your diets")
            break;
            case "groceries":
                this.handleComponentSwitch("groceries", "Your groceries for this week")
            break;
            case "settings":
                this.handleComponentSwitch("settings", "Account settings")
            break;
        }
    }

    handleComponentSwitch(componentToShow, title) {
        this.setState({
            activeComponent: componentToShow,
            pageTitle: title,
            showMenu: false
        })
    }

    render() {

        if(this.state.activeComponent === "recipes" || this.state.activeComponent === "groceries" || this.state.activeComponent === "settings") {
            document.getElementsByClassName("component-display")[0].classList.add("with-filter");

        }

        return (
            <div className="index">
                <MenuBar>
                    <div className="menu-toggle" onClick={this.handleMenu}>
                        <div className="stripe"> </div>
                        <div className="stripe"> </div>
                        <div className="stripe"> </div>
                    </div>
                    <span className="welcome-user"> Menu </span>
                </MenuBar>
                <HeaderBox>
                    <h1> {this.state.pageTitle} </h1>
                </HeaderBox>
                {this.state.showMenu ? (
                    <Menu>             
                        <div onClick={this.handleMenu} className="black-overlay">  </div> 
                    </Menu>
                ) : (
                    null
                )}
                
                <div className={"component-display"}> 
                    <div className="center">
                        <Recipes showScreen={this.state.activeComponent} activeDay={this.props.match.params.filter} userId={this.state.userId} />
                        <Allergies userId={this.state.userId} showScreen={this.state.activeComponent} />
                        <Diets userId={this.state.userId} showScreen={this.state.activeComponent} />
                        <Groceries userId={this.state.userId} showScreen={this.state.activeComponent} activeDay={this.props.match.params.filter} />
                        <Settings userId={this.state.userId} showScreen={this.state.activeComponent} activeComponent={this.props.match.params.filter} />
                    </div>
                </div>
            </div>
        );
    }
}

export default Dashboard;