import React from 'react';
import { NavLink } from "react-router-dom";
import MenuBar from './compositionComponents/MenuBar';
import { getCurrentDayName } from '../../Main';
import IngredientList from './compositionComponents/Ingredients';
import InstructionList from './compositionComponents/Instructions';
import NutritionList from './compositionComponents/Nutrition';


class FullRecipe extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            day: "",
            title: "",
            image: "",
            dishTypes: [],
            time: "",
            servings: "",
            ingredientsCount: "",
            ingredients: [],
            instructions: [],
            nutrition: {},
        }
    }
    

    componentDidMount() {
       let recipes = JSON.parse(localStorage.getItem("Schedule"))
       recipes.forEach(recipe => {
           if(recipe.id === this.state.id) {
               this.setState({
                    day: recipe.day,
                    title: recipe.title,
                    image: recipe.image,
                    dishTypes: recipe.dishType,
                    time: recipe.servingTime,
                    servings: recipe.servings,
                    ingredientsCount: recipe.ingredients.length,
                    ingredients: recipe.ingredients,
                    instructions: recipe.instructions,
                    nutrition: recipe.nutrients
               })
           }
       });
    }

    render() {
        return (
            <div className="recipe-full">
                <MenuBar>
                    <NavLink to={"/recipes/filter="+getCurrentDayName()}> <i className="far fa-chevron-left"></i> <span className="action-index"> Back to all recipes </span> </NavLink>
                </MenuBar>
                <div className="center-base">   
                    <div className="current-recipe">
                        <div className="recipe-img">
                            <img src={this.state.image} />
                        </div>
                        <div className="recipe-details">
                            <div className="space-x">
                                <span className="day-tag"> {this.state.day} </span>
                                <h3> {this.state.title} </h3>
                                <ul className="tags">
                                    <li> {this.state.dishTypes[3]} </li>
                                    <li> {this.state.dishTypes[1]} </li>
                                </ul>
                                <ul className="specs"> 
                                    <li> <i className="far fa-clock"></i> {this.state.time} minutes </li>
                                    <li> <i className="fas fa-users"></i> {this.state.servings} servings </li>
                                    <li>  <i className="far fa-carrot"></i>  {this.state.ingredientsCount} ingredients </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="details-box">
                        {this.state.day !== "" ? (
                            <IngredientList ingredients={this.state.ingredients} day={this.state.day} />
                        ) : (
                            null
                        )}
                        {this.state.instructions.length > 0 ? (
                            <InstructionList instructions={this.state.instructions} />
                        ) : (
                            null
                        )}
                        {this.state.nutrition.length > 0 ? (
                            <NutritionList nutrition={this.state.nutrition} />
                        ) : (
                            null
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

export default FullRecipe;