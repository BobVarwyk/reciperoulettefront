import React from 'react';
import Filter from '../compositionComponents/Filter';
import Axios from 'axios';
import RecipeCard from '../compositionComponents/RecipeCard';
import { NavLink } from "react-router-dom";
import RecipeCardSkeleton from '../compositionComponents/RecipeCardSkeleton';


class Recipes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            recipes: [],
            activeDay: "",
            scheduleActive: true,
            activeRecipe: {},
            error: false,
            errorMessage: "",
            loading: true
        }
    }
    

    componentDidMount() {
        if(localStorage.getItem("Schedule") !== null){
            this.requestRecipeInfoFromStorage();
        } else {
            this.requestRecipeInfoFromApi();
        }
    }

    componentWillReceiveProps(props) {
        this.setState({
            activeDay: props.activeDay
        }, () => {
            this.showActiveRecipe();
        })
    }

    // Get the recipe data from the REST API if localstorage isn't set.
    async requestRecipeInfoFromApi() {
        Axios.get(`${process.env.REACT_APP_BACKEND_API}/${this.props.userId}/recipe/get/all`)
        .then(response => {
            if(!response.data.succes) {
                this.setState({
                    error: response.data.succes,
                    errorMessage: response.data.message
                })
                this.changeActiveLoadingState(false)
            } else {
                // If schedule is expired, don't set the schedule state
                if(this.isScheduleExpired(response.data.expireDate)) {
                    this.setState({
                        loading: false
                    })
                    return null;
                }
                // If not expired, store the recipes in the state
                this.changeActiveLoadingState(true)
                this.setState({
                    recipes: response.data.message,
                })
                // Also save it in localStorage
                localStorage.setItem("Schedule", JSON.stringify(response.data.message))
                localStorage.setItem("ScheduleExpireDate", JSON.stringify(response.data.expireDate))
            }
        }).then(() => {
            this.showActiveRecipe()
        }).catch((error) => {
            this.setState({
                error: false,
                errorMessage: error.message
            })
        })
    }

    requestRecipeInfoFromStorage() {
        // Parse localstorage to JSON 
        let schedule = JSON.parse(localStorage.getItem("Schedule"))
        
        // Check if schedule is still valid
        if(this.isScheduleExpired(JSON.parse(localStorage.getItem("ScheduleExpireDate")))) {
            this.changeActiveLoadingState(false);
            return null;
        }
        // Set state with the info from Storage
        this.changeActiveLoadingState(true);
        this.setState({
            recipes: schedule
        }, () => {
            this.showActiveRecipe()
        })
    }

    handleChange(event) {
        const {name, value} = event.target;
        // Set the state with the values in input boxes
        this.setState({
            [name]: value
        })
    }

    changeActiveLoadingState(status) {
        this.setState({
            scheduleActive: status,
            loading: false
        })
    }

    // Check if schedule is still valid
    isScheduleExpired(dateFromJson) {
        // Convert state expireDate to Javascript data
        let expirationDate = new Date(dateFromJson)
        // Get the currentDate
        let currentDate = new Date()
        let returnValue = null
        if(currentDate > expirationDate) {
            returnValue = true
        } else {
            returnValue = false
        }
        return returnValue
    }

    showActiveRecipe() {
        this.state.recipes.forEach((recipe) => {
            if(recipe.day.toString().toLowerCase() === this.state.activeDay) {
                this.setState({
                    activeRecipe: recipe,
                    loading: false
                })
            }
        })
    }

    render() {
        
        
        if(this.props.showScreen !== "recipes") {
            return null;
        }


        return (
            
            <div className="full-width">
                {!this.state.scheduleActive ? (
                    <div className="request-schedule">
                        <p> You do not have an active schedule for the upcoming seven days, please generate a new schedule via the button underneath. Remember that the recipes will be for the next 7 days and can't be changed inbetween. 
                            <br /> <br /> <strong> Remember to fill in your allergies & diet according to your living situation. These will be factors in the selection of the recipes. </strong>
                        </p>
                        <NavLink className="btn" to="/schedule/new"> Create a new schedule </NavLink>
                    </div>
                ) : (
                    <div className="col-md">
                        <Filter>
                            <li> <NavLink id="monday" to={"/recipes/filter=monday"}> monday </NavLink> </li>
                            <li> <NavLink id="tuesday" to={"/recipes/filter=tuesday"}> tuesday </NavLink> </li>
                            <li> <NavLink id="wednesday" to={"/recipes/filter=wednesday"}> wednesday </NavLink> </li>
                            <li> <NavLink id="thursday" to={"/recipes/filter=thursday"}> thursday </NavLink> </li>
                            <li> <NavLink id="friday" to={"/recipes/filter=friday"}> friday </NavLink> </li>
                            <li> <NavLink id="saturday" to={"/recipes/filter=saturday"}> saturday </NavLink> </li>
                            <li> <NavLink id="sunday" to={"/recipes/filter=sunday"}> sunday </NavLink> </li>
                        </Filter>
                        <div className="recipe">
                            {this.state.loading ? (
                                <RecipeCardSkeleton />
                            ) : (
                                <RecipeCard recipe={this.state.activeRecipe} />
                            )}
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

export default Recipes;