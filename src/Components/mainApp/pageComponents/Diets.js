import React from 'react';
import Axios from 'axios';
import OutputResponseMessage from '../../../generalComponents/OutputResponseMessage';
import { throwStatement } from '@babel/types';

class Diets extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            diets: ["Gluten", "Ketogenic", "Vegetarian", "Lacto-Vegetarian", "Ovo-Vegetarian", "Vegan", "Pescetarian", "Paleo", "Primal", "Whole30"],
            activeDiet: "",
            error: false,
            errorMessage: ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.saveDietApi = this.saveDietApi.bind(this);
    }

    componentDidMount() {
        Axios.get(`${process.env.REACT_APP_BACKEND_API}/diets/${this.props.userId}`)
        .then(response => {
            if(response.data.succes) {
                this.setActiveDiet(response.data.message)
            } else {
                this.setMessageState(response.data.succes, response.data.message)
            }
        }).catch((error) => {
            this.setMessageState(false, error.message)
        })
    }


    handleChange(event) {
        const {name, value} = event.target;
        // Set the state with the values in input boxes
        this.setState({
            [name]: value
        })
    }

    setActiveDiet(diet) {
        this.setState({
            activeDiet: diet
        })
    }

    setMessageState(status, message) {
        this.setState({
            error: status,
            errorMessage: message
        })
    }

    saveDietApi(event) {
        const { value } = event.target;
        Axios.put(`${process.env.REACT_APP_BACKEND_API}/account/diets/${this.props.userId}`, {
            dietName: value
        }).then(response => {
            if(response.data.succes) {
               this.setActiveDiet(response.data.message)
            } else {
                this.handleErrorState(response.data.succes, response.data.message)
            }
        }).catch((error) => {
            this.handleErrorState(false, error.message)
        })
    }

    render() {

        if(this.props.showScreen !== "diets") {
            return null;
        }

        let dietCheckboxes = []
        this.state.diets.forEach(diet => {
            dietCheckboxes.push(
                <li key={diet}> 
                    {diet}
                    <label key={diet} className="switch">
                        <input onChange={this.saveDietApi} value={diet} type="checkbox" checked={this.state.activeDiet === diet}/>
                        <span className="slider round"></span>
                    </label>
                </li>
            )
        })

        return (
            <div className="full-width">
                <p className="intro-text"> You can disable or enable diet on this page, these allergies will be taken into account the next time new recipes will be generated for you. You are free to change your allergies as many times as you want. </p>
                <ul className="toggle-box">
                    { dietCheckboxes }
                </ul>
                <OutputResponseMessage succes={this.state.error} handler={this.handleChange}>
                    {this.state.errorMessage}
                </OutputResponseMessage>
            </div>
        );
    }
}

export default Diets;