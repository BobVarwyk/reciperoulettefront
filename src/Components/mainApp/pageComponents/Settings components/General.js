import React from 'react';
import Axios from 'axios';
import TextInput from '../../../../generalComponents/TextInput';
import OutputResponseMessage from '../../../../generalComponents/OutputResponseMessage';

class GeneralSettings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: "",
            lastName: "",
            email: "",
            houseHoldSize: 0,
            error: false,
            errorMessage: ""
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleResponseState = this.handleResponseState.bind(this)
    }

    componentDidMount() {
        Axios.get(`${process.env.REACT_APP_BACKEND_API}/account/${this.props.userId}`)
        .then(response => {
            this.setState({
                firstName: response.data.firstName,
                lastName: response.data.lastName,
                email: response.data.email,
                houseHoldSize: response.data.householdSize
            })
        }).catch((error) => {
            this.handleResponseState(false, error.message)
        })
    }

    handleChange(event) {
        const {name, value} = event.target;
        // Set the state with the values in input boxes
        this.setState({
            [name]: value
        })
    }

    handleResponseState(status, message) {
        this.setState({
            error: status,
            errorMessage: message
        })
    }

    handleSubmit(event) {
        event.preventDefault();
        Axios.put(`${process.env.REACT_APP_BACKEND_API}/account/update/${this.props.userId}`, {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            houseHoldSize: this.state.houseHoldSize
        }).then(response => {
            this.handleResponseState(response.data.succes, response.data.message)
        }).catch((error) => {
            this.handleResponseState(false, error.message)
        })
    }

    render() {    
        return (
            <div>
                <p> You can edit your general account information on this page, please remember your old settings cannot be retrieved after changing them. </p>
                <form>
                    <label> First name </label>
                    <TextInput handlechange={this.handleChange} value={this.state.firstName} name="firstName" type="text" />
                    <label> Last name </label>
                    <TextInput handlechange={this.handleChange} value={this.state.lastName} name="lastName" type="text" />
                    <label> Email </label>
                    <TextInput handlechange={this.handleChange} value={this.state.email} name="email" type="email" />
                    <label> Household size </label>
                    <TextInput handlechange={this.handleChange} value={this.state.houseHoldSize} name="houseHoldSize" type="number" />
                    <button onClick={this.handleSubmit} type="submit" className="btn"> Save changes </button>
                </form>
                <OutputResponseMessage succes={this.state.error} handler={this.handleChange}>
                    {this.state.errorMessage}
                </OutputResponseMessage>
            </div>
        );
    }
}

export default GeneralSettings;