import React from 'react';
import Axios from 'axios';
import TextInput from '../../../../generalComponents/TextInput';
import OutputResponseMessage from '../../../../generalComponents/OutputResponseMessage';

class EditPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            oldPassword: "",
            newPassword: "",
            passwordConfirm: "",
            error: false,
            errorMessage: ""
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleResponseState = this.handleResponseState.bind(this)
    }

    handleChange(event) {
        const {name, value} = event.target;
        // Set the state with the values in input boxes
        this.setState({
            [name]: value
        })
    }

    handleResponseState(status, message) {
        this.setState({
            error: status,
            errorMessage: message
        })
    }

    handleSubmit(event) {
        event.preventDefault();
        Axios.put(`${process.env.REACT_APP_BACKEND_API}/account/update/password/${this.props.userId}`, {
            oldPassword: this.state.oldPassword,
            password: this.state.newPassword,
            passwordConfirm: this.state.passwordConfirm
        }).then(response => {
            this.handleResponseState(response.data.succes, response.data.message)
        }).catch((error) => {
            this.handleResponseState(false, error.message)
        })
    }

    render() {    
        return (
            <div>
                <p> You can change your password on this page, keep in mind that when you change your password the next time you log in it will acquire the new password. </p>
                <form>
                    <label> Old password </label>
                    <TextInput handlechange={this.handleChange} name="oldPassword" type="password" />
                    <label> New password </label>
                    <TextInput handlechange={this.handleChange} name="newPassword" type="password" />
                    <label> Confirm password </label>
                    <TextInput handlechange={this.handleChange} name="passwordConfirm" type="password" />
                    <button disabled={this.state.oldPassword === "" || this.state.newPassword === "" || this.state.passwordConfirm === ""} onClick={this.handleSubmit} type="submit" className="btn"> Change password </button>
                </form>
                <OutputResponseMessage succes={this.state.error} handler={this.handleChange}>
                    {this.state.errorMessage}
                </OutputResponseMessage>
            </div>
        );
    }
}

export default EditPassword;