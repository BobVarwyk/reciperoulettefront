import React from 'react';
import Axios from 'axios';
import TextInput from '../../../../generalComponents/TextInput';
import OutputResponseMessage from '../../../../generalComponents/OutputResponseMessage';
import { stat } from 'fs';

class DeleteAccount extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: "",
            error: false,
            errorMessage: ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleChange(event) {
        const {name, value} = event.target;
        // Set the state with the values in input boxes
        this.setState({
            [name]: value
        })
    }

    handleResponseState(status, message) {
        this.setState({
            error: status,
            errorMessage: message
        })
    }

    handleDelete(event) {
        event.preventDefault();
        Axios.delete(`${process.env.REACT_APP_BACKEND_API}/account/delete/${this.props.userId}`, {
            data: {password: this.state.password}
        }).then(response => {
            this.handleResponseState(response.data.succes, response.data.message)
            if(response.data.succes) {
                setTimeout(() => {
                    localStorage.clear();
                    window.location.replace("/");
                }, 2500);
            }
        }).catch((error) => {
            this.handleResponseState(false, error.message)
        })
    }

    render() {    
        return (
            <div>
                <p> You can delete your account if you no longer use recipe roulette, keep in mind that your account will be deleted permanently. There is no way to retrieve your account after deletion.
                    To make sure only you can delete your account, it is required to fill in your current password before you can delete your account. </p>
                <form>
                    <label> Current password </label>
                    <TextInput handlechange={this.handleChange} name="password" type="password" /> 
                    <button disabled={this.state.password === ""} onClick={this.handleDelete} type="submit" className="btn"> <span> Yes, delete my account </span> </button>
                </form>
                <OutputResponseMessage succes={this.state.error} handler={this.handleChange}>
                    {this.state.errorMessage}
                </OutputResponseMessage>
            </div>
        );
    }
}

export default DeleteAccount;