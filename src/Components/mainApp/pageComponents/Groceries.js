import React from 'react';
import Filter from '../compositionComponents/Filter';
import Axios from 'axios';
import OutputResponseMessage from '../../../generalComponents/OutputResponseMessage';
import { AnimateOnChange } from 'react-animation';
import { NavLink } from "react-router-dom";
import GroceryCard from '../compositionComponents/GroceryCard';


class Groceries extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeDay: "monday",
            groceries: [],
            activeGroceries: {},
            error: false,
            errorMessage: "",
            loading: true
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleGroceryChange = this.handleGroceryChange.bind(this);
    }
    

    componentDidMount() {
        Axios.get(`${process.env.REACT_APP_BACKEND_API}/groceries/${this.props.userId}/get/all`)
        .then(response => {
            if(response.data.succes) {
                this.setState({
                    groceries: response.data.message,
                })
            } else {
                this.setErrorState(response.data.succes, response.data.message)
            }
        }).then(() => {
            this.showActiveGroceries();
        }).catch((error) => {
            this.setErrorState(false, error.message)
        })
    }

    componentWillReceiveProps(props) {
        this.setState({
            activeDay: props.activeDay
        }, () => {
            this.showActiveGroceries();
        })
    }

    setErrorState(status, message) {
        this.setState({
            error: status, 
            errorMessage: message
        })
    }

    handleChange(event) {
        const {name, value} = event.target;
        // Set the state with the values in input boxes
        this.setState({
            [name]: value
        })
    }

    showActiveGroceries() {
        if(this.state.groceries !== undefined) {
            for(var dayKey in this.state.groceries) {
                // Get the day selected
                if(dayKey === this.state.activeDay) {
                    var selectedGroceries = this.state.groceries[dayKey];
                    this.setState({
                        activeGroceries: selectedGroceries
                    })
                }
            }
        }
    }

    handleGroceryChange(event) {
        const {value} = event.target;
        Axios.put(`${process.env.REACT_APP_BACKEND_API}/groceries/${this.props.userId}/update/${this.state.activeDay}`, {
            groceryItem: value
        }).then(response => {
            this.setErrorState(response.data.succes, response.data.message)
        }).catch((error) => {
            this.setErrorState(false, error.message)
        })
    }



    render() {
        
        
        if(this.props.showScreen !== "groceries") {
            return null;
        }

        // Get the groceries of the selected day
        const groceries = [];
        if(this.state.activeGroceries.length > 0) {
            this.state.activeGroceries.forEach(element => {
                groceries.push(
                    <li key={element}> 
                        <label htmlFor={element} className="grocery-item"> {element} </label>
                        <input onChange={this.handleGroceryChange} type="checkbox" id={element} name={element} value={element} /> 
                        <span className="checkmark"> <i className="fa fa-check"></i> </span>
                    </li>
                )
            });
        }


        return (
            <div className="full-width">
                <Filter>
                    <li> <NavLink id="monday" to={"/groceries/filter=monday"}> monday </NavLink> </li>
                    <li> <NavLink id="tuesday" to={"/groceries/filter=tuesday"}> tuesday </NavLink> </li>
                    <li> <NavLink id="wednesday" to={"/groceries/filter=wednesday"}> wednesday </NavLink> </li>
                    <li> <NavLink id="thursday" to={"/groceries/filter=thursday"}> thursday </NavLink> </li>
                    <li> <NavLink id="friday" to={"/groceries/filter=friday"}> friday </NavLink> </li>
                    <li> <NavLink id="saturday" to={"/groceries/filter=saturday"}> saturday </NavLink> </li>
                    <li> <NavLink id="sunday" to={"/groceries/filter=sunday"}> sunday </NavLink> </li>
                </Filter>
                <GroceryCard day={this.state.activeDay}>
                    {this.state.activeGroceries.length > 0 ? (
                        groceries
                    ) : (
                        <span> It seems like you haven't selected any groceries yet for {this.state.activeDay}, you can add groceries to your list by visiting the recipe of this day.  </span>
                    )}
                </GroceryCard>
                <OutputResponseMessage succes={this.state.error} handler={this.handleChange}>
                    {this.state.errorMessage}
                </OutputResponseMessage>
            </div>
        );
    }
}

export default Groceries;