import React from 'react';
import Axios from 'axios';
import OutputResponseMessage from '../../../generalComponents/OutputResponseMessage';

class Allergies extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            allergies: ["Dairy", "Peanut", "Soy", "Egg", "Seafood", "Sulfite", "Gluten", "Sesame", "Tree nut", "Grain", "Shellfish", "Wheat"],
            activeAllergies: [],
            error: false,
            errorMessage: ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleAllergySave = this.handleAllergySave.bind(this);
    }

    componentDidMount() {
        Axios.get(`${process.env.REACT_APP_BACKEND_API}/allergies/${this.props.userId}`)
        .then(response => {
            if(response.data.succes) {
                this.handleActiveAllergiesState(response.data.message)
            } else {
                this.handleErrorState(response.data.succes, response.data.message)
            }
        }).catch((error) => {
            this.handleErrorState(false, error.message)
        })
    }

    handleErrorState(errorStatus, errorMsg) {
        this.setState({
            error: errorStatus,
            errorMessage: errorMsg
        })
    }

    handleActiveAllergiesState(allergies) {
        this.setState({
            activeAllergies: allergies
        })
    }

    handleChange(event) {
        const {name, value} = event.target;
        // Set the state with the values in input boxes
        this.setState({
            [name]: value
        })
    }

    handleAllergySave(event) {
        const { value } = event.target;
        Axios.put(`${process.env.REACT_APP_BACKEND_API}/account/allergies/${this.props.userId}`, {
            allergyName: value
        }).then(response => {
            if(response.data.succes) {
               this.setState({
                   activeAllergies: response.data.message,
               })
            } else {
                this.handleErrorState(response.data.succes, response.data.message)
            }
        }).catch((error) => {
            this.handleErrorState(false, error.message)
        })
    }

    render() {

        if(this.props.showScreen !== "allergies") {
            return null;
        }

        let allergiesCheckboxes = []
        this.state.allergies.forEach(allergy => {
            allergiesCheckboxes.push(
                <li key={allergy}> 
                    {allergy}
                    <label key={allergy} className="switch">
                        <input onChange={this.handleAllergySave} value={allergy} type="checkbox" checked={this.state.activeAllergies.includes(allergy)}/>
                        <span className="slider round"></span>
                    </label>
                </li>
            )
        })

        return (
            <div className="full-width">
                <p className="intro-text"> You can disable or enable allergies on this page, these allergies will be taken into account the next time new recipes will be generated for you. You are free to change your allergies as many times as you want. </p>
                <ul className="toggle-box">
                    { allergiesCheckboxes }
                </ul>
                <OutputResponseMessage succes={this.state.error} handler={this.handleChange}>
                    {this.state.errorMessage}
                </OutputResponseMessage>
            </div>
        );
    }
}

export default Allergies;