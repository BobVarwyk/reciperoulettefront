import React from 'react';
import Filter from '../compositionComponents/Filter';
import { NavLink } from "react-router-dom";
import GeneralSettings from './Settings components/General';
import EditPassword from './Settings components/Password';
import DeleteAccount from './Settings components/DeleteAccount';


class Settings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeComponent: "",
        }
    }

    componentDidMount() {
        this.setState({
            activeComponent: this.props.activeComponent
        })
    }
    
    componentDidUpdate() {
        this.setState({
            activeComponent: this.props.activeComponent
        })
    }

    shouldComponentUpdate(prevProps) {
        if(prevProps.activeComponent !== this.state.activeComponent) {
            return true;
        }
        return false;
    }


    handleResponseState(status, message) {
        this.setState({
            error: status,
            errorMessage: message
        })
    }

    render() {
        
        
        if(this.props.showScreen !== "settings") {
            return null;
        }

        return (
            <div className="full-width">
                <Filter>
                    <li> <NavLink id="general" to={"/settings/filter=general"}> General account information </NavLink> </li>
                    <li> <NavLink id="password" to={"/settings/filter=password"}> Edit password </NavLink> </li>
                    <li> <NavLink id="delete-account" to={"/settings/filter=delete-account"}> Delete account </NavLink> </li>
                </Filter>
                <div className="settings-content">
                    {this.state.activeComponent === "general" ? (
                        <GeneralSettings userId={this.props.userId} />
                    ) : this.state.activeComponent === "password" ? (
                        <EditPassword userId={this.props.userId} />
                    ) : (
                        <DeleteAccount userId={this.props.userId} />
                    )}
                </div>
            </div>
        );
    }
}

export default Settings;