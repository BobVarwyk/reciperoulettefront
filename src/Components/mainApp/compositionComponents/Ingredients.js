import React from 'react';
import Axios from 'axios';
import OutputResponseMessage from '../../../generalComponents/OutputResponseMessage';
import { NavLink } from "react-router-dom";

class IngredientList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: JSON.parse(localStorage.getItem("User")).id,
            day: "",
            ingredients: [],
            groceries: [],
            error: false,
            errorMessage: ""
        }
        this.handleGroceries = this.handleGroceries.bind(this);
        this.saveGroceries = this.saveGroceries.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        let day = this.props.day
        let ingredients = this.props.ingredients
        let trimmedIngredients = []

        ingredients.map((item) => {
            trimmedIngredients.push(Math.round(item.amount * 100) / 100 + " " + item.unit + " " + item.name)
        })

        if(ingredients.length > 0 && day !== "") {
            this.setState({
                day: day,
                ingredients: ingredients,
                groceries: trimmedIngredients
            })
        }
    }

    handleGroceries(event) {
        // Get the value of the clicked checkbox.
        const { value } = event.target;
        // Check if item already is active in groceries array, if not add it to array, if so delete it from the array
        if(!this.state.groceries.some(item => value === item)) {
            this.setState({
                groceries: [...this.state.groceries, value]
            })
        } else {
            let itemToRemove = this.state.groceries.indexOf(value);
            this.setState({
                groceries: this.state.groceries.filter((_, i) => i !== itemToRemove),
            })
        }
    }

    saveGroceries() {
        Axios.put(`${process.env.REACT_APP_BACKEND_API}/groceries/${this.state.userId}/update`, {
            day: this.state.day.toLowerCase(),
            groceries: this.state.groceries
        }).then(response => {
            this.setState({
                error: response.data.succes,
                errorMessage: response.data.message
            })
        })
    }

    handleChange(event) {
        const {name, value} = event.target;
        // Set the state with the values in input boxes
        this.setState({
            [name]: value
        })
    }

    render() {

        // Get all the allergies from state and show them in the return of render
        let IngredientList = [];
        this.state.ingredients.forEach(ingredient => {
            let ingredientItem = Math.round(ingredient.amount * 100) / 100 + " " + ingredient.unit + " " + ingredient.name;
            IngredientList.push(
                <li className="checkbox" key={ingredient.id + Math.random(0, 100)}>
                    <input onChange={this.handleGroceries} type="checkbox" id={ingredient.name} value={ingredientItem} checked={this.state.groceries.includes(ingredientItem)}/> 
                    <span className="checkmark"> <i className="fa fa-check"></i> </span>
                    <label htmlFor={ingredientItem} className="grocery-item"> {ingredientItem} </label>
                </li>
            )
        })

        return (
            <div className="list" id="ingredient-list">
                <h4> Ingredients </h4>
                <ul>
                    {IngredientList}
                </ul>
                <a onClick={this.saveGroceries} className="btn"> Add to groceries </a>
                <OutputResponseMessage succes={this.state.error} handler={this.handleChange}>
                    {this.state.errorMessage}
                </OutputResponseMessage>
            </div>
        );
    }
}

export default IngredientList;