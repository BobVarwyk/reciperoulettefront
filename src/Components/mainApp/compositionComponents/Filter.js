import React from 'react';

function Filter(props) {
    return (
        <div className="filter-bar-y">
            <ul>
                {props.children}
            </ul>
        </div>
    );
}

export default Filter;