import React from 'react';

function GroceryCard(props) {

    return (
        <div className="grocery-list">
            <p> Your groceries for the upcoming weekschedule, filtered by the day they are needed on. You can add groceries on the recipe page. On this page you can delete groceries by clicking on the box on the right. </p>
            <h3 className="day-title"> {props.day} </h3>
            <ul className="groceries-list">
                {props.children}
            </ul>
        </div>
    );
}

export default GroceryCard;