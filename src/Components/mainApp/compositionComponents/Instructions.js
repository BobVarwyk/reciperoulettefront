import React from 'react';

function InstructionList(props) {

    let instructions = []
    props.instructions.forEach(instruction => {
        instruction.steps.forEach(step => {
            instructions.push(
                <li key={step.number}> <div className="instruction-key"> {step.number} </div> <span> {step.step} </span> </li>
            )
        })
    });

    return (
        <div className="list" id="instructions-list">
            <h4> Instructions </h4>
            <ul className="instructions">
                {instructions}
            </ul>
        </div>
    );
}

export default InstructionList;