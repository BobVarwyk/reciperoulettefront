import React from 'react';
import { NavLink } from "react-router-dom";
import { getCurrentDayName } from '../../../Main';

function Menu(props) {

    function signOut() {
        localStorage.clear()
        window.location.replace("/")
    }

    return (
        <div>
            <div className="menu" id="main-menu">
                <div className="back-to-index"> <div className="center"> <NavLink to="/recipes"> <i className="fas fa-home-lg-alt"></i> </NavLink> </div> </div>
                <div className="center">
                    <ul id="menu-items">
                        <li> <NavLink to={"/recipes/filter="+getCurrentDayName()}> <i className="fas fa-utensils"></i> <span> Recipes </span></NavLink> </li>
                        <li> <NavLink to="/allergies"> <i className="fal fa-wheat"></i> <span> Allergies </span> </NavLink> </li>
                        <li> <NavLink to="/diets"> <i className="fas fa-french-fries"></i> <span> Diets </span> </NavLink> </li>
                        <li> <NavLink to={"/groceries/filter="+getCurrentDayName()}> <i className="fas fa-sticky-note"></i> <span> Groceries </span></NavLink> </li>
                        <li> <NavLink to="/settings/filter=general"> <i className="fas fa-cog"></i> <span> Settings </span> </NavLink> </li>
                        <li> <button onClick={signOut} id="logout"> <i className="fal fa-sign-out-alt"></i> <span> Sign out </span> </button> </li>
                    </ul>
                </div>
            </div>
            {props.children}
        </div>
    );
}

export default Menu;