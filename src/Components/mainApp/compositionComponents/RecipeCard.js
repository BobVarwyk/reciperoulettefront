import React from 'react';
import { NavLink } from "react-router-dom";

function RecipeCard(props) {
    
    // Get 6 ingredients of the total ingredient list
    let ingredients = []
    if(props.recipe.ingredients !== undefined) {
        props.recipe.ingredients.slice(0, 6).forEach(element => {
            ingredients.push(
                <li key={element.id + Math.random(0, 100)}> {element.amount + " " + element.unit + " " + element.name } </li>
            )
        });
    }

    // Get these specific nutrients from all nutrients
    let nutrients = {
        Calories: "",
        Carbohydrates: "",
        Protein: "",
        Fat: ""
    }

    if(props.recipe.nutrients !== undefined) {
        props.recipe.nutrients.forEach((nutrient) => {
            let amountOfNutrient = Math.round(nutrient.amount);
            switch(nutrient.title) {
                case "Calories":
                    nutrients.Calories = amountOfNutrient;
                break;
                case "Carbohydrates":
                    nutrients.Carbohydrates = amountOfNutrient;
                break;
                case "Protein":
                    nutrients.Protein = amountOfNutrient;
                break;
                case "Fat":
                    nutrients.Fat = amountOfNutrient;
                break;
            }
        })
    }


    return (
        <div className="current-recipe">
            <div className="recipe-img">
                <img src={props.recipe.image} />
            </div>
            <div className="recipe-details">
                <div className="space-x">
                    <span className="day-tag"> {props.recipe.day} </span>
                    <h3> {props.recipe.title} </h3>
                    <ul className="specs"> 
                        <li> <i className="fal fa-utensils"></i> Main course </li>
                        <li> <i className="far fa-clock"></i> {props.recipe.servingTime} minutes </li>
                        <li> <i className="fas fa-users"></i> {props.recipe.servings} servings </li>
                    </ul>
                    <NavLink className="btn" to={"/recipe/" + props.recipe.id}> View recipe </NavLink>
                </div>
            </div>
            <div className="recipe-extended-details" id="ingredient-detail">
                <div className="center-y">
                    <h4> Ingredients </h4>
                    <ul> 
                        {ingredients}
                        <li> * All ingredients can be found on the matching recipe page </li>
                    </ul>
                </div>
            </div>
            <div className="recipe-extended-details" id="nutrient-detail">
                <div className="center-y">
                    <h4> Nutrition </h4>
                    <ul className="nutrient-list">
                        <li> <span> Energy (Kcal) </span> <span> {nutrients.Calories} </span> </li>
                        <li> <span> Carbohydrates (Gr) </span> <span> {nutrients.Carbohydrates} </span> </li>
                        <li> <span> Protein (Gr) </span> <span> {nutrients.Protein} </span> </li>
                        <li> <span> Fat (Gr) </span> <span> {nutrients.Fat} </span> </li>
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default RecipeCard;