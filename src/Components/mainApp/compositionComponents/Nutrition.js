import React from 'react';

function NutritionList(props) {

    let nutrients = {
        Calories: "",
        Carbohydrates: "",
        Protein: "",
        Fat: ""
    }

    // Loop through all nutrients and select the values needed in the switch statement. 
    props.nutrition.forEach((nutrient) => {
        let amountOfNutrient = Math.round(nutrient.amount);
        switch(nutrient.title) {
            case "Calories":
                nutrients.Calories = amountOfNutrient;
            break;
            case "Carbohydrates":
                nutrients.Carbohydrates = amountOfNutrient;
            break;
            case "Protein":
                nutrients.Protein = amountOfNutrient;
            break;
            case "Fat":
                nutrients.Fat = amountOfNutrient;
            break;
        }
    })

    return (
        <div className="list" id="nutrition-list">
            <h4> Nutrition </h4>
            <ul>
                <li> <span>  </span> <span> <strong> Per serving </strong> </span> </li>
                <li> <span> Energy (Kcal) </span> <span> {nutrients.Calories} </span> </li>
                <li> <span> Carbohydrates (Gr) </span> <span> {nutrients.Carbohydrates} </span> </li>
                <li> <span> Protein (Gr) </span> <span> {nutrients.Protein} </span> </li>
                <li> <span> Fat (Gr) </span> <span> {nutrients.Fat} </span> </li>
            </ul>
        </div>
    );
}

export default NutritionList;