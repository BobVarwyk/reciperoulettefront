import React from 'react';
import { NavLink } from "react-router-dom";

function RecipeCardSkeleton(props) {
    return (
        <div className="current-recipe">
            <div className="recipe-img">
                <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div>
            </div>
            <div className="recipe-details">
                <div className="space-x">
                    <h3 className="skeleton-frame-l"> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </h3>
                    <ul> 
                        <li className="skeleton-frame-m-space"> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                        <li className="skeleton-frame-m-space"> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                        <li className="skeleton-frame-m-space"> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                    </ul>
                    <NavLink className="btn" to={"/"}> <li> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li> </NavLink>
                </div>
            </div>
            <div className="recipe-extended-details" id="ingredient-detail">
                <div className="center-y">
                    <h4 className="skeleton-frame-l"> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </h4>
                    <ul> 
                        <li className="skeleton-frame-m"> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                        <li className="skeleton-frame-m"> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                        <li className="skeleton-frame-m"> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                        <li className="skeleton-frame-m"> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                    </ul>
                </div>
            </div>
            <div className="recipe-extended-details" id="nutrient-detail">
                <div className="center-y">
                    <h4 className="skeleton-frame-l"> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </h4>
                    <ul className="nutrient-list">
                        <li> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                        <li> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                        <li> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                        <li> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default RecipeCardSkeleton;