import React from 'react';

function MenuBar(props) {
    return (
        <div className="top-ui-bar" id="top-bar">
            <div className="center">
                {props.children}
            </div>
        </div>
    );
}

export default MenuBar;