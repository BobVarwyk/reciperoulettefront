import React from 'react';

function HeaderBox(props) {
    return (
        <div className="header-box">
            <div className="center">
                {props.children}
            </div>
        </div>
    );
}

export default HeaderBox;