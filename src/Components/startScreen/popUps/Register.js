import React from 'react';
import ProgressBar from '../../../generalComponents/ProgressBar';
import RegisterCredentials from './RegisterCredentials';
import RegisterAllergies from './RegisterAllergies';
import RegisterHousehold from './RegisterHousehold';
import RegistrationFinished from './RegistrationFinished';

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        currentStep:1,
    };
  }

  // Set currentStep one up on click previousButton
  nextStep() {
    let currentStep = this.state.currentStep;
    // Als de huidige stap 1 of 2 is, voeg dan 1 toe bij de volgende Button klik
    currentStep = currentStep >= 3? 4: currentStep +1;
    this.setState({
        currentStep: currentStep
    })
  }

  // Set currentStep one back on click previousButton
  previousStep() {
    let currentStep = this.state.currentStep;
    // Als de huidige stap 2 of 3 is, haal dan 1 eraf bij de volgende Button klik
    currentStep = currentStep <= 1? 1: currentStep - 1;
    this.setState({
        currentStep: currentStep
    })
  }


  render() {

    if(this.props.showScreen !== "register") {
      return null;
    }

    return (
        <div className="registration-proces">
            <ProgressBar currentStep={this.state.currentStep} />
            <RegisterCredentials currentStep={this.state.currentStep} actions={{ next: this.nextStep.bind(this), previous: this.previousStep.bind(this) }} />
            <RegisterAllergies currentStep={this.state.currentStep} actions={{ next: this.nextStep.bind(this), previous: this.previousStep.bind(this) }} />
            <RegisterHousehold currentStep={this.state.currentStep} actions={{ next: this.nextStep.bind(this), previous: this.previousStep.bind(this) }} />
            <RegistrationFinished currentStep={this.state.currentStep} actions={{ next: this.nextStep.bind(this), previous: this.previousStep.bind(this) }} />
        </div>
    );
  }
}

export default Register;