import React from 'react';
import { NavLink } from "react-router-dom";
import TextInput from '../../../generalComponents/TextInput';
import Axios from 'axios';
import OutputResponseMessage from '../../../generalComponents/OutputResponseMessage';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            buttonValue: "Sign in",
            isLoading: false,
            error: false,
            errorMessage: ""
        }
        this.handleLogin = this.handleLogin.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const {name, value} = event.target;
        // Set the state with the values in input boxes
        this.setState({
            [name]: value
        })
    }

    handleLogin() {
        Axios.post(`${process.env.REACT_APP_BACKEND_API}/login`, {
            email: this.state.email,
            password: this.state.password
        }).then(response => {
            if(response.data.succes) {
                // Set localstorage to the user logged in
                window.localStorage.setItem("User", JSON.stringify(response.data.message));
                // Redirect to the dasboard of the app
                window.location.replace("/");
            } else {
                // Set error state
                this.setState({
                    error: response.data.succes,
                    errorMessage: response.data.message
                })
            }
        }).catch((error) => {
            this.setState({
                error: false,
                errorMessage: error.message
            })
        })
    }

    render() {

        if(this.props.showScreen !== "login") {
            return null;
        }

        return (
           <div className="popup-content">
                <label> Email </label>
                <TextInput handlechange={this.handleChange} name="email" type="email" />
                <label> Password <NavLink to="/passwordrecovery" className="reset-link"> Forgot password? </NavLink> </label>
                <TextInput handlechange={this.handleChange} name="password" type="password" />
                <div className="actions">
                    <a className="btn" onClick={this.handleLogin}> {this.state.buttonValue} </a>
                    <NavLink to="/register"> No account? Sign up </NavLink>
                </div>
                <OutputResponseMessage succes={this.state.error} handler={this.handleChange}>
                    {this.state.errorMessage}
                </OutputResponseMessage>
           </div>
        );
    }
}

export default Login;