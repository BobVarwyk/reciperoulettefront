import React from 'react';
import { NavLink } from "react-router-dom";
import TextInput from '../../../generalComponents/TextInput';
import Axios from 'axios';
import OutputResponseMessage from '../../../generalComponents/OutputResponseMessage';

class Activation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            token: this.props.token,
            buttonValue: "Resend email",
            responseSucces: false,
            responseMessage: "",
            error: false,
            errorMessage: ""
        }
        this.handleChange = this.handleChange.bind(this)
        this.resendEmail = this.resendEmail.bind(this);
    }

    componentDidMount() {
        Axios.put(`${process.env.REACT_APP_BACKEND_API}/account/activate/${this.state.token}`, {})
        .then(response => {
            this.setState({
                responseSucces: response.data.succes,
                responseMessage: response.data.message
            })
        })
    }

    // For the closing of the output message
    handleChange(event) {
        const {name, value} = event.target;
        this.setState({
        [name]: value
        });
    }

    resendEmail() {
        Axios.post(`${process.env.REACT_APP_BACKEND_API}/account/activate/resend/${this.state.token}`, {})
        .then(response => {
            console.log(response)
            this.setState({
                error: response.data.succes,
                errorMessage: response.data.message
            })
        }).catch((error) => {
            this.setState({
                error: false,
                errorMessage: error.message
            })
        })
    }


    render() {

        if(this.props.showScreen !== "activation") {
            return null;
        }

        return (
           <div className="popup-content">
                <h2> {this.state.responseMessage} </h2>
                {!this.state.responseSucces ? (
                    <div>
                        <p> Oh no! something wen't wrong, we can't activate your account via this link, please generate a new one by clicking the resend button. </p>
                        <div className="actions no-margin">
                            <a className="btn" onClick={this.resendEmail}> {this.state.buttonValue} </a>
                        </div>
                    </div>
                ) : (
                    <div>
                        <p> Congratuliations, your account has been activated. You can now use every functionality of Recipe Roulette. </p>
                        <div className="actions">
                            <NavLink className="btn" to="/login"> Sign in to your account </NavLink>
                        </div>
                    </div>
                )}
                <OutputResponseMessage succes={this.state.error} handler={this.handleChange}>
                    {this.state.errorMessage}
                </OutputResponseMessage>
           </div>
        );
    }
}

export default Activation;