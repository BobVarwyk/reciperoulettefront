import React from 'react';
import AccountStore from '../../../Redux/Store/AccountStore';
import { setHousehold } from '../../../Redux/Actions/AccountCredentials';
import Axios from 'axios';
import OutputResponseMessage from '../../../generalComponents/OutputResponseMessage';

class RegisterHousehold extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonValue: "Finish registration",
            householdSize: 1,
            error: true,
            errorMessage: ""
        }
        this.handleHouseholdChange = this.handleHouseholdChange.bind(this);
        this.saveUser = this.saveUser.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleHouseholdChange(event) {
        var type = event.target.id
        var houseHoldSize = this.state.householdSize;
        
        if(type === "plus") {
            if(houseHoldSize < 12) {
                var newSize = houseHoldSize + 1;
                this.setState({
                    householdSize : newSize
                })
            }
        } else {
            if(houseHoldSize > 1) {
                var newSize = houseHoldSize - 1;
                this.setState({
                    householdSize : newSize
                })
            }
        }
    }

    async saveUser() {
        if(AccountStore.dispatch(setHousehold(this.state.householdSize))) {
            let accountBody = ({...AccountStore.getState().newAccount[0], ...AccountStore.getState().newAccount[1], ...AccountStore.getState().newAccount[2]});
            Axios.post(`${process.env.REACT_APP_BACKEND_API}/register`, {
                firstName: accountBody.firstName,
                lastName: accountBody.lastName,
                email: accountBody.email,
                password: accountBody.password,
                houseHoldSize: accountBody.houseHoldSize,
                allergies: accountBody.allergies
            }).then(response => {
                if(response.data.succes) {
                    this.props.actions.next();
                } 
                this.setState({
                    error: response.data.succes,
                    errorMessage: response.data.message
                })
            }).catch((error) => {
                this.setState({
                    error: false,
                    errorMessage: error.message
                })
            })
        } else {
            this.setState({
                error: true,
                errorMessage: "Error, we can't save your account at this time"
            })
        }
    }

    // For the closing of the output message
    handleChange(event) {
        const {name, value} = event.target;
        this.setState({
        [name]: value
        });
    }

    render() {

        if(this.props.currentStep !== 3) {
            return null;
        }

        return (
            <div className="popup-content">
                <p> For us to select recipes that are appropriate for your household, please fill in the size of your household. </p>
                <div className="selectors">
                    <a onClick={this.handleHouseholdChange} id="min" className="btn"> - </a>
                    <span className="house-size"> {this.state.householdSize} </span>
                    <a onClick={this.handleHouseholdChange} id="plus" className="btn"> + </a>
                </div>
                <div className="actions">
                    <a className="btn" onClick={this.saveUser}> {this.state.buttonValue} </a>
                    <a onClick={this.props.actions.previous}> Previous step </a>
                </div>
                <OutputResponseMessage succes={this.state.error} handler={this.handleChange}>
                    {this.state.errorMessage}
                </OutputResponseMessage>
            </div>
        );
    }
}

export default RegisterHousehold;