import React from 'react';
import AccountStore from '../../../Redux/Store/AccountStore';
import { setAllergies } from '../../../Redux/Actions/AccountCredentials';

class RegisterAllergies extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonValue: "Next step : Household size",
            allergies: ["Dairy", "Peanut", "Soy", "Egg", "Seafood", "Sulfite", "Gluten", "Sesame", "Tree nut", "Grain", "Shellfish", "Wheat"],
            activeAllergies: []
        }
        this.handleAllergiesChange = this.handleAllergiesChange.bind(this);
        this.storeAllergies = this.storeAllergies.bind(this);
    }

    handleAllergiesChange(event) {
        // Get the value of the clicked checkbox.
        const { value } = event.target;
        // Check if item already is active in groceries array, if not add it to array, if so delete it from the array
        if(!this.state.activeAllergies.some(item => value === item)) {
            this.setState({
                activeAllergies: [...this.state.activeAllergies, value]
            })
        } else {
            let itemToRemove = this.state.activeAllergies.indexOf(value);
            this.setState({
                activeAllergies: this.state.activeAllergies.filter((_, i) => i !== itemToRemove),
            })
        }
    }

    storeAllergies() {
        AccountStore.dispatch(setAllergies(this.state.activeAllergies));
        this.props.actions.next();
    }

    render() {

        if(this.props.currentStep !== 2) {
            return null;
        }

        // Get all the allergies from state and show them in the return of render
        let allergies = [];
        this.state.allergies.forEach(allergy => {
            allergies.push(
                <li className="checkbox" key={allergy}>
                    <input onChange={this.handleAllergiesChange} type="checkbox" id={allergy} value={allergy} checked={this.state.activeAllergies.includes(allergy)}/> 
                    <span className="checkmark"> <i className="fa fa-check"></i> </span>
                    <label htmlFor={allergy} className="grocery-item"> {allergy} </label>
                </li>
            )
        })

        return (
            <div className="popup-content">
                <p> Do you suffer from any of the allergies underneath? please select them so that we can select recipes that fit your current health situation. </p>
                <div className="selectors">
                    { allergies }
                </div>
                <div className="actions">
                    <a className="btn" onClick={this.storeAllergies}> {this.state.buttonValue} </a>
                    <a onClick={this.props.actions.previous}> Previous step </a>
                </div>
            </div>
        );
    }
}

export default RegisterAllergies;