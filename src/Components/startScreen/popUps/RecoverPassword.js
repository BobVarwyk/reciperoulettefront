import React from 'react';
import { NavLink } from "react-router-dom";
import TextInput from '../../../generalComponents/TextInput';
import Axios from 'axios';
import OutputResponseMessage from '../../../generalComponents/OutputResponseMessage';

class RecoverPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            buttonValue: "Recover password",
            error: false,
            errorMessage: ""
        }
        this.handleReset = this.handleReset.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const {name, value} = event.target;
        // Set the state with the values in input boxes
        this.setState({
            [name]: value
        })
    }

    handleReset() {
        Axios.post(`${process.env.REACT_APP_BACKEND_API}/account/password/reset`, {
            email: this.state.email
        }).then(response => {
            this.setState({
                error: response.data.succes,
                errorMessage: response.data.message
            })
        }).catch((error) => {
            this.setState({
                error: false,
                errorMessage: error.message
            })
        })
    }

    render() {

        if(this.props.showScreen !== "recoverpass") {
            return null;
        }

        return (
           <div className="popup-content">
                <label> Email </label>
                <TextInput handlechange={this.handleChange} name="email" type="email" />
                <div className="actions">
                    <a className="btn" onClick={this.handleReset}> {this.state.buttonValue} </a>
                    <NavLink to="/login"> Sign in to your account </NavLink>
                </div>
                <OutputResponseMessage succes={this.state.error} handler={this.handleChange}>
                    {this.state.errorMessage}
                </OutputResponseMessage>
           </div>
        );
    }
}

export default RecoverPassword;