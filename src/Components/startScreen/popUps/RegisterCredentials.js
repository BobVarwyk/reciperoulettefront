import React from 'react';
import { NavLink } from "react-router-dom";
import TextInput from '../../../generalComponents/TextInput';
import { validateFormFields } from '../../../Main';
import AccountStore from '../../../Redux/Store/AccountStore';
import { setCredentials } from '../../../Redux/Actions/AccountCredentials';

class RegisterCredentials extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: "",
            lastName: "",
            email: "",
            password: "",
            passwordConfirm: "",
            buttonValue: "Next step : Allergies",
            errors: []
        }
        this.saveCredentials = this.saveCredentials.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const {name, value} = event.target;
        // Set the state with the values in input boxes
        this.setState({
            [name]: value
        })
    }

    saveCredentials() {
        const errors = validateFormFields(this.state);
        if(errors.firstName === null && errors.lastName === null && errors.email === null && errors.password === null && errors.passwordConfirm === null) {
            AccountStore.dispatch(setCredentials(this.state.firstName, this.state.lastName, this.state.email, this.state.password));
            this.props.actions.next();    
        } else {
            this.setState({
                errors: errors
            })
        }
    }

    render() {

        if(this.props.currentStep !== 1) {
            return null;
        }

        return (
           <div className="popup-content">
                <label> Voornaam </label>
                <TextInput handlechange={this.handleChange} name="firstName" type="text" value={this.state.firstName} error={this.state.errors.firstName} />
                <label> Achternaam </label>
                <TextInput handlechange={this.handleChange} name="lastName" type="text" value={this.state.lastName}  error={this.state.errors.lastName} />
                <label> Email </label>
                <TextInput handlechange={this.handleChange} name="email" type="email" value={this.state.email}  error={this.state.errors.email} />
                <label> Wachtwoord </label>
                <TextInput handlechange={this.handleChange} name="password" type="password" value={this.state.password}  error={this.state.errors.password} />
                <label> Wachtwoord </label>
                <TextInput handlechange={this.handleChange} name="passwordConfirm" type="password" value={this.state.passwordConfirm}  error={this.state.errors.passwordConfirm} />
                <div className="actions">
                    <a className="btn" onClick={this.saveCredentials}> {this.state.buttonValue} </a>
                    <NavLink to="/login"> Sign in to your account </NavLink>
                </div>
           </div>
        );
    }
}

export default RegisterCredentials;