import React from 'react';
import { NavLink } from "react-router-dom";
import TextInput from '../../../generalComponents/TextInput';
import Axios from 'axios';
import OutputResponseMessage from '../../../generalComponents/OutputResponseMessage';
import { timingSafeEqual } from 'crypto';

class PasswordRecover extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            token: this.props.token,
            password: "",
            passwordConfirm: "",
            buttonValue: "Recover your password",
            error: false,
            errorMessage: ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }

    // For the closing of the output message
    handleChange(event) {
        const {name, value} = event.target;
        this.setState({
        [name]: value
        });
    }

    handleReset() {
        Axios.put(`${process.env.REACT_APP_BACKEND_API}/account/password/reset/${this.state.token}`, {
        oldPassword: this.state.password,
        password: this.state.password,
        passwordConfirm: this.state.passwordConfirm
        }).then(response => {
            this.setState({
                error: response.data.succes,
                errorMessage: response.data.message
            })
        }).catch((error) => {
            this.setState({
                error: false,
                errorMessage: error.message
            })
        })
    }

    render() {

        if(this.props.showScreen !== "passwordrecovery") {
            return null;
        }

        return (
           <div className="popup-content">
                <label> Password  </label>
                <TextInput handlechange={this.handleChange} name="password" type="password" />
                <label> Password confirmation  </label>
                <TextInput handlechange={this.handleChange} name="passwordConfirm" type="password" />
                <div className="actions">
                    <a className="btn" onClick={this.handleReset}> {this.state.buttonValue} </a>
                    <NavLink to="/login"> Sign in to your account </NavLink>
                </div>
                <OutputResponseMessage succes={this.state.error} handler={this.handleChange}>
                    {this.state.errorMessage}
                </OutputResponseMessage>
           </div>
        );
    }
}

export default PasswordRecover;