import React from 'react';
import AccountStore from '../../../Redux/Store/AccountStore';
import Axios from 'axios';
import OutputResponseMessage from '../../../generalComponents/OutputResponseMessage';

class RegistrationFinished extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonValue: "Resend email",
            householdSize: 1,
            error: true,
            errorMessage: ""
        }
        this.resendMail = this.resendMail.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    async resendMail() {
        const body = ({...AccountStore.getState().newAccount[0]});
        Axios.post(`${process.env.REACT_APP_BACKEND_API}/account/activate/resend`, {
            email: body.email,
        }).then(response => {
            if(response.data.succes) {
                this.props.actions.next();
            } 
            this.setState({
                error: response.data.succes,
                errorMessage: response.data.message
            })
        }).catch((error) => {
            this.setState({
                error: false,
                errorMessage: error.message
            })
        })
    }

     // For the closing of the output message
     handleChange(event) {
        const {name, value} = event.target;
        this.setState({
        [name]: value
        });
    }

    render() {

        if(this.props.currentStep !== 4) {
            return null;
        }

        return (
            <div className="popup-content">
                <div className="finished">
                    <h2> Check your mailbox for an activation link </h2>
                    <p> All you need to do now is activate your account through the mail we've send you. <br /> <br /> Didn't receive our mail? send it again. </p>
                </div>
                <div className="actions">
                    <a className="btn" onClick={this.resendMail}> {this.state.buttonValue} </a>
                </div>
                <OutputResponseMessage succes={this.state.error} handler={this.handleChange}>
                    {this.state.errorMessage}
                </OutputResponseMessage>
            </div>
        );
    }
}

export default RegistrationFinished;