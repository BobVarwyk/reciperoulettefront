import React from 'react';
import { NavLink } from "react-router-dom";

function PopUp(props) {
    return (
      <div className="dark-overlay">
         <div className="pop-up-modal">
            <div className="space-y">
                <div className="pop-up-actions"> <h4> {props.title} </h4> <NavLink to="/"> <i className="fal fa-times"></i> </NavLink> </div>
                {props.children}
            </div>
         </div>
      </div>
    );
}

export default PopUp;