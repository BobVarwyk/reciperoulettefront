import React from 'react';
import { NavLink } from "react-router-dom";
import logo from '../../images/logoWit.svg';
import PopUp from './compositionComponents/popUp';
import Login from './popUps/Login';
import RecoverPassword from './popUps/RecoverPassword';
import Register from './popUps/Register';
import Activation from './popUps/ActivationStatus';
import PasswordRecover from './popUps/ResetPassword';


class StartScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showScreen: "",
            popUpTitle: "",
            loading: true
        }
    }

    componentDidMount() {
        this.assignPopUp(this.props.match.url);
    }
    
    componentWillReceiveProps(props) {
        this.assignPopUp(props.match.url);
    }

    assignPopUp(URL) {
        switch(URL) {
            case "/login":
                this.handleComponentSwitch("login", "Sign in");
            break;
            case "/register":
                this.handleComponentSwitch("register", "Register");
            break;
            case "/passwordrecovery":
                this.handleComponentSwitch("recoverpass", "Recover password");
            break;
            default:
                this.handleComponentSwitch("", "");
        }
        // Activation screen
        if(this.props.match.params.token != null) {
            this.handleComponentSwitch('activation', "Activate your account");
        }
        // Password reset screen
        if(this.props.match.params.resettoken != null) {
            this.handleComponentSwitch('passwordrecovery', "Reset your password");
        }
    }

    handleComponentSwitch(componentToShow, componentTitle) {
        this.setState({
            showScreen: componentToShow,
            popUpTitle: componentTitle,
            loading: false
        })
    }

    render() {
        return (
            !this.state.loading ? (
                <div className="startScreen">
                    <div className="header">
                        <img src={logo} />
                    </div>
                    <div className="actions-container">
                        <div className="x-space">
                            <h1> Maak kennis met nieuwe recepten! </h1>
                            <NavLink className="btn" to="register"> Registreren </NavLink>
                            <NavLink className="btn" to="login"> inloggen </NavLink>
                        </div>
                        {this.state.popUpTitle !== "" ? (
                            <PopUp title={this.state.popUpTitle}> 
                                <Login showScreen={this.state.showScreen} />
                                <RecoverPassword showScreen={this.state.showScreen} />
                                <Register showScreen={this.state.showScreen} />
                                <Activation showScreen={this.state.showScreen} token={this.props.match.params.token} />
                                <PasswordRecover showScreen={this.state.showScreen} token={this.props.match.params.resettoken} />
                            </PopUp>
                        ) : (
                            null
                        )}
                    </div>
                </div>
            ) : (
                null
            )
        );
    }
}

export default StartScreen;